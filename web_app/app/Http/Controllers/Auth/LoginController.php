<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\User;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function index(){
        return view('site.login');
    }

    public function handleLogin(){

        $data = (object) $_POST;
        var_dump($data);

        $user = User::whereNested(function($query) use ($data){

            $query->where('EMAIL' , '=', trim($data->email));
            $query->where('PASS' , '=', trim($data->password));

        })->first();

        var_dump($user);

        /*if( count($user) > 0 ){

            Auth::login($user);
            // return redirect( route('game-index') );
            return redirect( route('login-welcome') );
        }

        return redirect( route('login') )->with('status','Nombre de usuario o contraseña incorrectos');*/

    }

}
