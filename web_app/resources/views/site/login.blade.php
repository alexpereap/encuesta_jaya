@extends('layouts.default')
@section('title', 'Iniciar sesión')

@section('style')
    <link href="{{ asset('./css/signin.css') }}" rel="stylesheet">
@endsection

@section('body')
<div class="container">

  <form class="form-signin" method="POST" action="{{ route('authenticate') }}">
  {{csrf_field()}}
  <h2 class="form-signin-heading">Iniciar sesión</h2>
    <label for="inputEmail" class="sr-only">Correo electrónico</label>
    <input type="email" id="inputEmail" name="email" class="form-control" placeholder="Correo electrónico" required autofocus>
    <label for="inputPassword" class="sr-only">Contraseña</label>
    <input type="password" id="inputPassword" name="password" class="form-control" placeholder="Contraseña" required>
    <button class="btn btn-lg btn-primary btn-block" type="submit">Ingresar</button>
</form>

</div> <!-- /container -->
@endsection