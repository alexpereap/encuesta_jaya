<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Encuesta Jaya - @yield('title')</title>
    <!-- Bootstrap core CSS -->
    <link href="{{ asset('./bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    @yield('style')
    @yield('header_scripts')
    
  </head>

  <body>
  @yield('body')

   <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="{{ asset('./js/jquery-1.12.4.min.js') }}"></script>
    <script src="{{ asset('./js/proper-1.11.0.min.js') }}"></script>
    <script src="{{ asset('./bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('./js/ie10-viewport-bug-workaround.js') }}"></script>
    @yield('footer_scripts')
  </body>
</html>
